const url = "https://api-gateway.skymavis.com/graphql/marketplace"; // Reemplaza con la URL real de tu servidor GraphQL
const urlMarketplace = "https://app.axieinfinity.com/marketplace/axies"

let maximo = 81;
let from = 0;
let size = 20;

let tbodyhtml = '';

const divisor = 1000000000000000000

let compraTotal = 0
let ventaEsperada = 0
let contadorAxieEnVenta = 0
let contadorAxieFalta = 0
let contadorAxie = 0
let contadorOfertas = 0


async function consulta(size, from) {
    let graphqlQuery = `
    query MyQuery {
      axies(
        owner: "0x6e36f58007b4a8396922d28906f72cdcd0401722"
        sort: IdAsc
        from: ${from}
        size: ${size}
      ) {
        total
        results {
          id
          image
          name
          transferHistory {
            total
            results {
              withPrice
              withPriceUsd
              timestamp
              from
            }
          }
          breedCount
          order {
            currentPrice
            endedPrice
            currentPriceUsd
            basePrice
          }
          offers {
            data {
              currentPrice
              currentPriceUsd
            }
          }
        }
      }
    }
  `;

    try {
        const response = await fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "X-API-Key": "SaF5CQBpwFELqdEjyDtaTe4JmDYQuf5B",
            },
            body: JSON.stringify({ query: graphqlQuery }),
        });
        const data = await response.json();
        // console.log(data);

        data.data.axies.results?.forEach((v, i) => {
          if(v.offers.data.length > 0){

            contadorAxie += 1
            tbodyhtml += `
            <tr>
              <th scope="row">${contadorAxie}</th>
              <td><a href="${urlMarketplace}/${v.id}/" target="_blank">${v.id}</a></td>
              <td><img src="${v.image}" alt="image axie" width="100" /></td>
              <td>${v.name} (${v.breedCount})</td>
              <td>`;
                v.transferHistory.results?.forEach((va, j) => {
                    tbodyhtml += `
                    <p>${changeDate(va.timestamp)}</p>
                    <p>${(va.withPrice/divisor).toFixed(6)} (${Number(va?.withPriceUsd ?? 1).toFixed(4)})</p>
                    `;
                  });
                tbodyhtml += `
              </td>
              <td>${Number(v.transferHistory.results[0]?.withPriceUsd ?? 1).toFixed(4)}</td>`

              //compra de axie (suma)
              compraTotal += (v.transferHistory.results[0]?.withPrice ?? 1)/divisor

              if(v.order != null){
                contadorAxieEnVenta += 1

                tbodyhtml += `
                <td>
                  <p>${Number(v.order?.basePrice/divisor).toFixed(6)}</p>
                  <p>${Number(v.order?.currentPrice/divisor).toFixed(6)} (${Number(v.order?.currentPriceUsd).toFixed(4)})</p>
                  <p>${Number(v.order?.endedPrice/divisor).toFixed(6)}</p>
                </td>
                <td>${v.order?.endedPrice == 0 ? Number(v.order?.currentPrice/divisor).toFixed(6) : Number(v.order?.endedPrice/divisor).toFixed(6)}</td>`;

                ventaEsperada += v.order?.endedPrice == 0 ? v.order?.currentPrice/divisor :v.order?.endedPrice/divisor 
              }else{
                contadorAxieFalta += 1

                tbodyhtml += `<td></td><td></td>`;
              }

                contadorOfertas += 1
                tbodyhtml += `
                <td>
                `
                v.offers.data.map((val,k) => {
                  tbodyhtml += `<p>${Number(val.currentPrice/divisor).toFixed(6)} (${Number(val.currentPriceUsd).toFixed(4)})</p>`
                })
                tbodyhtml += `
                </td>
                `
              
              tbodyhtml += `</tr>`;
        }
        });

    } catch (error) {
        console.error("Error:", error);
    }
}

async function consultar() {
    await consulta(size, from);
    if (maximo > 0) {
        maximo -= size;
        from += size;
        await consultar(); // Esperar a que se complete la consulta antes de continuar con la recursión
    } else {
        // console.log(tbodyhtml);
        const tbody = document.getElementById("tbody");
        tbody.innerHTML = tbodyhtml; // Actualizar el HTML después de completar todas las consultas


        const informacion = document.getElementById("informacion")
        informacion.innerHTML = `
                            <p>Compra: ${compraTotal}</p>
                            <p>Axies en Venta: ${contadorAxieEnVenta}</p>
                            <p>Axies que falta listar: ${contadorAxieFalta}</p>
                            <p>Venta final: ${ventaEsperada}</p>
                            <p>Ofertas disponibles: ${contadorOfertas}</p>
                            `
    }
}

function changeDate(v_fecha){
  const timestamp = v_fecha; // timestamp en segundos
  const fecha = new Date(timestamp * 1000); // Multiplicamos por 1000 para convertir el timestamp a milisegundos

  const dia = fecha.getDate().toString().padStart(2, '0'); // Obtenemos el día del mes y lo formateamos
  const mes = (fecha.getMonth() + 1).toString().padStart(2, '0'); // Obtenemos el mes (los meses van de 0 a 11) y lo formateamos
  const año = fecha.getFullYear(); // Obtenemos el año

  const fechaFormateada = `${dia}/${mes}/${año}`;
  return fechaFormateada
  }

consultar();
